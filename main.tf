# vnet locals
locals {
  prefix_name = var.prefix_name
  subnets_info = { for index, subnet in var.subnets :
    subnet.name => subnet.cidr != "" ? subnet.cidr : cidrsubnet(var.vnet_cidr[0], 8, index)
  }
  tags = {
    project = var.project
    owner   = var.owner
  }
}

# creation du reseau virtuel
resource "azurerm_virtual_network" "main" {
  name                = "${local.prefix_name}-vnet"
  location            = var.vnet_location
  resource_group_name = var.resource_group_name
  address_space       = var.vnet_cidr
  dns_servers         = var.dns_servers_vnet
  tags                = local.tags
}

# creation des sous-reseaux
resource "azurerm_subnet" "main" {
  for_each             = local.subnets_info
  name                 = "${local.prefix_name}-snet-${each.key}"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_virtual_network.main.resource_group_name
  address_prefixes     = [each.value]
}

resource "azurerm_network_security_group" "subnet_nsg" {
  count               = var.create_nsg ? 1 : 0
  name                = var.subnet_nsg_name
  location            = var.vnet_location
  resource_group_name = var.resource_group_name
}

resource "azurerm_subnet_network_security_group_association" "subnet_nsg_association" {
  for_each = var.create_nsg ? azurerm_subnet.main : {}

  subnet_id                 = each.value.id
  network_security_group_id = azurerm_network_security_group.subnet_nsg[0].id
}

