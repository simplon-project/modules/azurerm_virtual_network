output "vnet_location" {
  description = "La région Azure où les ressources ont été créées"
  value       = azurerm_virtual_network.main.location
}
output "virtual_network_id" {
  description = "L'id de la ressource du réseau virtuel Azure"
  value       = azurerm_virtual_network.main.id
}
output "virtual_network_name" {
  description = "Le nom de la ressource du réseau virtuel Azure"
  value       = azurerm_virtual_network.main.name
}
output "virtual_network_address_space" {
  description = "L'espace d'adressage du réseau virtuel Azure"
  value       = azurerm_virtual_network.main.address_space
}
output "subnet_ids" {
  value       = { for name, subnet in azurerm_subnet.main : name => subnet.id }
  description = "Les IDs des sous-réseaux Azure"
}
# output "vnet_subnets_ids" {
#   value = {
#     for key, subnet in azurerm_subnet.main : key => subnet.id
#   }
# }
output "subnet_address_prefixes" {
  value       = { for name, subnet in azurerm_subnet.main : name => subnet.address_prefixes }
  description = "Les préfixes d'adresse des sous-réseaux Azure"
}
