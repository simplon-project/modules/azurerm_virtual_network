variable "resource_group_name" {
  description = "Nom du groupe de ressources"
  type        = string
}
variable "vnet_location" {
  description = "region Azure où les ressources seront créées"
  type        = string
}
variable "project" {
  description = "Nom du projet"
  type        = string
}
variable "owner" {
  description = "Nom du propriétaire du projet"
  type        = string
}
variable "prefix_name" {
  description = "Prefixe pour les noms des ressources créées"
  type        = string
}
variable "vnet_cidr" {
  description = "Cidr block de l'adresse IP du réseau virtuel"
  type        = list(string)
  default     = ["10.0.0.0/16"]
  validation {
    condition     = can([for cidr in var.vnet_cidr : cidrsubnet(cidr, 8, 0)])
    error_message = "Le bloc CIDR fourni n'est pas valide."
  }
}


variable "subnets" {
  description = "Liste des sous-réseaux à créer, chaque sous-réseau étant un objet avec un nom et un bloc CIDR"
  type = list(object({
    name = string
    cidr = string
  }))
  default = [
    {
      name = "snet1"
      cidr = ""
    },
    {
      name = "snet2"
      cidr = ""
    }
  ]
}
variable "dns_servers_vnet" {
  description = "Liste des serveurs DNS à utiliser pour le réseau virtuel"
  type        = list(string)
  default     = []

}
variable "create_nsg" {
  description = "Indique si un NSG doit être créé ou non"
  type        = bool
  default     = true
}
variable "subnet_nsg_name" {
  description = "Nom du groupe de sécurité du sous-réseau"
  type        = string
  default     = "subnet-nsg"
}
